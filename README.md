# r-dev-test

#### Steps to use this Gitpod Workspace
1. Sign Up for Gitpod

2. Inside Gitlab User Preference set Gitpod IDE for editing

(User Settings -> Preference -> Integrations -> Gitpod IDE checkbox)

3. Fork the repo(https://gitlab.com/AtharvaS08/r-dev-test/)

4. Click on Edit button above repo file (beside Code button(Green)).

5. Select Gitpod IDE.

6. Will be Redirected to Gitpod Dashboard and Image Building will start after couples of minutes Workspace will start with minimal R installation present in it.




#### Direct Method(usage without gitlab acc)

[![Gitpod](https://img.shields.io/badge/gitpod-f06611.svg?style=for-the-badge&logo=gitpod&logoColor=white)](https://gitpod.io/#gitlab.com/AtharvaS08/r-dev-test/)

This will need just Gitpod account.