FROM gitpod/workspace-full:latest

ENV BUILDDIR='/workspaces/r-dev-env/build'
ENV TOP_SRCDIR='/workspaces/r-dev-env/svn'

USER root
RUN apt-get update
RUN apt-get install -y r-base

USER gitpod
RUN R --version
